package models;

import POJO.Student;

import java.util.ArrayList;

public class StudentModel {
    //creating an Arraylist to store students information
    ArrayList<Student> students = new ArrayList();

    public void addInitialStudents(){
        //Calling the constructor with 10 students information

        students.add(new Student("Delilah Smith",1));
        students.add(new Student("Valerie Passant", 2));
        students.add( new Student("Amina Mubarak", 3));
        students.add( new Student("Angela Rogerio", 4));

        students.add(new Student("Emilia Clark", 5));
        students.add( new Student("Joseph Hadad", 6));
        students.add( new Student("Wasila Modif", 7));
        students.add( new Student("Pierre Dupond", 8));
        students.add( new Student("Tommy Stevenson", 9));
        students.add( new Student("Julio Gonzares", 10));

    }

    // printing all registered students
    public void viewAllStudents() {

        for (Student s : students) {
            System.out.println(s.studentName);
        }
    }

    //Checking if the student is a registered student
    public boolean isStudentRegistered(int studentId) {

        for(Student s:this.students) {

            if(s.getId()==studentId) {
                return true;
            }
        }
        return false;
    }

    public Student getStudent(int id) {

        for(Student s:students) {

            if(s.getId()==id) {
               return s;
           }
       }
       return null;
    }
/*
    public int getBookingID(int bookingId){
        for (Student s: students){
            if(s.getBookingId()==bookingId){
                return s.getBookingId();
            }
        }
        return bookingId;
    }
*/
    /*

    //attend a lesson function
   public String attendALesson(String bookingStatus){
      return getBookingStatus(bookingStatus);
   }




    public String getBookingStatus(String bookingStatus) {
        for (Student s : students) {
            if (s.getBookingStatus(bookingStatus)=="Attended") {
               return bookingStatus;

            } else if (s.getBookingStatus(bookingStatus)=="Booked"){
                s.getBookingStatus("Attended");

                return null;
            }

        } return null;
    }
    */

   // public Student setBookingStatus(String bookingStatus){  }



      //  public void addStudent(String name, int id){

         //   this.students.add(new Student(name, id));

       // }
    }

