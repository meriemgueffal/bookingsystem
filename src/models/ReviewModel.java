package models;

//import POJO.Rating;
import POJO.Review;


import java.awt.*;
import java.util.ArrayList;

public class ReviewModel {




    ArrayList<Review> Reviews = new ArrayList<>();





    public void addInitialReviews(){
        //1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied



        Review Review1 = new Review(2,"Zumba",3, "The class was fun but it was too intense for me. ");
        Review Review2 = new Review(3, "Pilates", 2, "Unprofessional staff. ");
        Review Review3 = new Review(9, "Yoga", 3,"I enjoyed the class. " );
        Review Review4 = new Review(2, "CrossFit", 4, "Everyone was energetic and friendly. ");
        Review Review5 = new Review(3, "CrossFit", 2, "There was no air condition in the room. ");

        Review Review6 = new Review(9, "Zumba", 5, "Excellent class and amazing staff. ");
        Review Review7 = new Review(5, "Yoga", 1, "The staff was rude. ");
        Review Review8 = new Review(1, "Pilates", 4, "Good price for such a class. ");
        Review Review9 = new Review(4, "Yoga", 3, "It would have been better if they told us to bring our own equipment in advance. ");
        Review Review10 = new Review(6, "Zumba", 2,"It could do with some improvements.");

        Review Review11 = new Review(8, "CrossFit", 5, "The class was brilliant. ");
        Review Review12 = new Review(8, "Pilates", 4, "I highly recommend. ");
        Review Review13 = new Review(1,"Zumba", 3, "Wonderful but needs organisation. ");
        Review Review14 = new Review(10, "Yoga", 2, "I had to workout without any equipment. ");
        Review Review15 = new Review(4, "Zumba", 5, "Great atmosphere and kind staff. ");

        Review Review16 = new Review(10, "Pilates", 5, "I love taking this class. ");
        Review Review17 = new Review(7, "CrossFit", 4, "I can't recommend these lessons enough. ");
        Review Review18 = new Review(7, "Zumba", 1, "Worst nightmare I ended up injuring myself! ");
        Review Review19 = new Review(5,"CrossFit",5,"Great workout and great staff. ");
        Review Review20 = new Review(6,"Yoga",3,"The class is great but the room is too small. ");

        Reviews.add(Review1);
        Reviews.add(Review2);
        Reviews.add(Review3);
        Reviews.add(Review4);
        Reviews.add(Review5);
        Reviews.add(Review6);
        Reviews.add(Review7);
        Reviews.add(Review8);
        Reviews.add(Review9);
        Reviews.add(Review10);
        Reviews.add(Review11);
        Reviews.add(Review12);
        Reviews.add(Review13);
        Reviews.add(Review14);
        Reviews.add(Review15);
        Reviews.add(Review16);
        Reviews.add(Review17);
        Reviews.add(Review18);
        Reviews.add(Review19);
        Reviews.add(Review20);


    }

    public void viewAllReviews() {

        for(int i=0;i< Reviews.size();i++) {
            System.out.println(Reviews.get(i));

        }
    }

    public void addReview(int studentId, String lessonName,int rating, String reviewText){

            Reviews.add(new Review(studentId, lessonName, rating, reviewText));
        }



    public void AVG() {

        //lessons average
        int yogaRatings = 0;
        int pilatesRatings = 0;
        int zumbaRatings = 0;
        int crossFitRatings = 0;

        //lessons count
        int yogaCount = 0;
        int pilatesCount = 0;
        int zumbaCount = 0;
        int crossFitCount = 0;

        for (Review r : Reviews) {
            if (r.getLessonName().equalsIgnoreCase("Yoga")) {
                yogaRatings += r.getRating();
                yogaCount++;
            } else if (r.getLessonName().equalsIgnoreCase("Pilates")) {
                pilatesRatings += r.getRating();
                pilatesCount++;
            } else if (r.getLessonName().equalsIgnoreCase("Zumba")) {
                zumbaRatings += r.getRating();
                zumbaCount++;
            } else if (r.getLessonName().equalsIgnoreCase("CrossFit")) {
                crossFitRatings += r.getRating();
                crossFitCount++;
            }
        }

        System.out.println("Yoga average rating: "+ yogaRatings/yogaCount);
        System.out.println("Pilates average rating: "+ pilatesRatings/pilatesCount);
        System.out.println("Zumba average rating: " + zumbaRatings/zumbaCount);
        System.out.println("CrossFit average rating: " + crossFitRatings/crossFitCount);

    }


}











