package models;

import java.util.Objects;
import java.util.Scanner;

public class MyController {

     StudentModel sModel;
     LessonModel lModel;
     MyClassModel cModel;
    ReviewModel rModel;
    //RatingModel raModel;

     Scanner scanner;

    public MyController() {
        sModel = new StudentModel();
        lModel = new LessonModel();
        cModel = new MyClassModel();
        rModel = new ReviewModel();



        sModel.addInitialStudents();

        lModel.addInitialLessons();

        cModel.addInitialClasses();

        rModel.addInitialReviews();


        scanner = new Scanner(System.in);
    }

     public void viewAllStudents(){
        sModel.viewAllStudents();
     }

    public void viewAllLessons(){
        lModel.viewAllLessons();
    }

     public void viewALLClasses()
    {
       cModel.viewALLClasses(sModel);
    }


    public void viewAllReviews(){ rModel.viewAllReviews();}
    public void AVG() {rModel.AVG();}


   /* public void addStudent() {
        System.out.println("Enter the Name of the student:");
        String name = scanner.nextLine();
        System.out.println("Enter the ID of the student:");
        int id = scanner.nextInt();
        System.out.println("Enter the booking ID:");
        int bookingId=scanner.nextInt();
        scanner.nextLine();
        System.out.println("Please enter the booking status:");
        String bookingStatus=scanner.nextLine();

        this.sModel.addStudent(name, id);
    }

    public void addNewLesson() {
        System.out.println("Please enter the lesson's name: ");
        String lessName = scanner.nextLine();

        System.out.println("Please enter the lesson's ID: ");
        int lessId = scanner.nextInt();

        System.out.println("Please enter the lesson's price: ");
        float lessPrice = scanner.nextFloat();


        this.lModel.addNewLesson(lessName, lessPrice);
    }

    public void addClasses() {
        //int lessonId,int shift,int day,Date sDate, POJO.Date eDate
        System.out.println("Please enter the lesson's ID: ");
        int lessonId = scanner.nextInt();

        System.out.println("Please enter the shift: ");
        int shift = scanner.nextInt();

        System.out.println("Please enter the day: ");
        String day = scanner.nextLine();
        System.out.println("Please enter the week number: ");
        int weekNumber=scanner.nextInt();
        System.out.println("Please enter the month number:");
        int monthNumber= scanner.nextInt();
        this.cModel.addClasses(lessonId, shift, day,weekNumber, monthNumber);
    } */

    //corresponds to canceling a booking:
    public  void removeStudentFromClass() {
        System.out.println("Please enter your booking id: ");
        int bookingId=scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter name  of the lesson: ");
        String lessonName = scanner.nextLine();

        System.out.println("Enter id of Student: ");
        int id = scanner.nextInt();
        if (cModel.isValidBookingID(bookingId)){

        } else {
            System.out.println("This booking ID doesn't exist!");
        }
        //check is student registered
        if(!sModel.isStudentRegistered(id))
        {
            System.out.println("This student with id "+id+ " is not found");
            return;
        }
        //check is lesson running or not

        if(!lModel.isLessonRegistered(lessonName))
        {
            System.out.println("This lesson "+lessonName+" is not registered..");
            return;
        }

        //if everything is found
        boolean canRemove = cModel.removeStudentFromLesson(lessonName,id);
        if(canRemove)
        {
            System.out.println("Student ID "+id+" is removed from Lesson"+lessonName);
        } else
        {
            System.out.println("Something is wrong");

        }

    }

    //corresponds to booking a group lesson:
    public void bookLesson() {

        while (true) {
            System.out.println("\nPlease choose an option from the menu below: ");
            System.out.println("1. Browse timetable by day.");
            System.out.println("2. Browse timetable by exercise name.");
            System.out.println("3. Exit.");

            int choice2 = scanner.nextInt();

            if (choice2 == 3) {
                break;
            }
            switch (choice2) {
                case 1:
                    scanner.nextLine();

                    // Choosing the day and returning the timetabled group exercise lessons accordingly.
                    System.out.println("Please enter a day (Saturday or Sunday): ");
                    String day = scanner.nextLine();

                    if (day.equals("Saturday")) {
                        cModel.viewSatClasses(sModel, day);
                    }
                    else
                        cModel.viewSunClasses(sModel, day);


                    // booking a lesson.

                    System.out.println("Enter name  of the lesson you want to book");
                    String lessonName = scanner.nextLine();
                    if (!lModel.isLessonRegistered(lessonName)) {
                        System.out.println("Lesson is not registered");
                        return;
                    }

                    System.out.println("Enter Student ID:");
                    int studentId = scanner.nextInt();

                    if (!sModel.isStudentRegistered(studentId)) {
                        System.out.println("Student is not registered");
                        return;
                    } else {

                        //ask shift

                        System.out.println("Enter the shift:");
                        int shift = scanner.nextInt();
                        System.out.println("Enter the week number:");
                        int weekNo = scanner.nextInt();
                        scanner.nextLine();
                        System.out.println("Please enter the  monthNumber:");
                        int monthNumber=scanner.nextInt();

                        cModel.bookStudent( lessonName, studentId, shift, day, weekNo, monthNumber);
                    }


                    break;
                case 2:

                    System.out.println("Please enter exercise name (Yoga, Zumba, CrossFit or Pilates): ");
                    scanner.nextLine();
                    String ExeName = scanner.nextLine();


                    if (ExeName.equals("Yoga")){
                    cModel.viewYogaClasses(sModel, ExeName);
                    }
                    else if (ExeName.equals("Zumba")){
                        cModel.viewZumbaClasses(sModel, ExeName);
                    }
                    else if (ExeName.equals("CrossFit")){
                        cModel.viewCrossFitClasses(sModel, ExeName);
                    }
                    else if (ExeName.equals("Pilates")){
                        cModel.viewPilatesClasses(sModel, ExeName);
                    }


                if (!lModel.isLessonRegistered(ExeName)) {
                    System.out.println("Lesson is not registered");
                    return;
                } else {
                    System.out.println("Enter the day:");


                    String day1 = scanner.nextLine();





                System.out.println("Enter Student ID:");
                int studentId1 = scanner.nextInt();

                if (!sModel.isStudentRegistered(studentId1)) {
                    System.out.println("Student is not registered");
                    return;
                } else {
                    System.out.println("Enter the shift: (for MORNING: 10, AFTERNOON: 11, and for EVENING = 12 )");
                   int shift = scanner.nextInt();
                    System.out.println("Enter the week number:");
                    int weekNo = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Please enter the  monthNumber:");
                    int monthNumber=scanner.nextInt();
                    cModel.bookStudent( ExeName, studentId1, shift,day1, weekNo, monthNumber);
                }
                }

                break;
            }
        }
    }

    public void attendClass(int bookId)
    {
        cModel.attendMyClass(bookId);
    }





    public void reviews(){

        rModel.viewAllReviews();


    }

    public void changeBooking(){

        System.out.println("Please Enter booking ID");
        int bookId = scanner.nextInt();
        if (cModel.isValidBookingID(bookId))
        {
            int studentId = cModel.getStudentIdByBookId(bookId);
            // ask for newLesson Name
            System.out.println("Please enter the lesson name:");
            String newLesson=scanner.nextLine();



            //ask for new day
            System.out.println("Please enter the day:");
            String day =scanner.nextLine();

            //ask for new shift
            System.out.println("Please enter the shift:");
            int shift =scanner.nextInt();

            //ask for new week no
            System.out.println("Please enter the week number:");
            int weekno =scanner.nextInt();
            //ask for new months no
            System.out.println("Please enter the month number:");
            int monthno =scanner.nextInt();

            if(cModel.isStudentAvailable(studentId, newLesson, shift, day, weekno, monthno))
            {
                if(cModel.isLessonRunning(newLesson, shift, day, weekno, monthno))
                {
                    if(cModel.isSpaceAvailable(newLesson, shift, day, weekno, monthno))
                    {
                        if (cModel.removeStudent_bookID(bookId)) {
                            cModel.bookStudent(bookId, newLesson,studentId, shift, day, weekno, monthno);
                        }

                    }
                }
            }
        }

        int studentID =1;//get this from book


        removeStudentFromClass();
        bookLesson();


    }


    public void addReview(){
        System.out.println("Enter id of Student");
        int studentId = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter the name of the Lesson");
        String lessonName = scanner.nextLine();

        System.out.println("Enter a numerical rating: 1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied.");
        int rating = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Enter your review:\n");
        String reviewText = scanner.nextLine();

        //check why it doesn't add reviews.
        rModel.addReview(studentId,lessonName,rating,reviewText);
        System.out.println("Your review was added successfully!");
    }

   /* public void attendLesson(){
        //System.out.println("Please enter the name of the lesson:");
        //System.out.println("Please enter your booking ID:");
         System.out.println("Please confirm your attendance by typing Yes or No.");
         String attendance= scanner.nextLine();
         if (attendance=="Yes"){
             sModel.getBookingStatus();
         } else if (attendance=="No"){
             sModel.getBookingStatus();
         }

    }*/

    // generating the lesson report for each month 1 and 2




    public void viewClassReport() {
        //prompt the user to input the month number (either 1 or 2)
        //System.out.println("Please enter a month number: (Please enter either 1 or 2.)");
        // int monthNumber =  scanner.nextInt();
        //use prompt to generate an according report (either for month 1 or month 2)


        //this is what I am struggling with. This code prints the report of two months combined
        //edit this code for above purpose
        cModel.viewReport1(sModel);
       //change avg() from rmodel to controller level
        AVG();

        System.out.println("\n\nAll the reviews with ratings");
        rModel.viewAllReviews();
    }




    //don't touch
    public void report2(){

        //prompt the user to input the month number (either 1 or 2)
        System.out.println("Please enter a month number: (Please enter either 1 or 2.)");
        //int monthNumber = scanner.nextInt();
        //use prompt to generate an according report (either for month 1 or month 2)


        System.out.println("generating the class report: \n");
        //problem
        cModel.viewReport2(sModel);

        //System.out.println("\n\nAll the reviews with ratings");
        //rModel.viewAllReviews();

    }


}



