package models;

import POJO.Lesson;

import java.util.ArrayList;

public class LessonModel {

    ArrayList<Lesson> LessonsCol = new ArrayList<Lesson>();



    public boolean isLessonRegistered(String lessonName) {
        for(Lesson l : LessonsCol)
        {
            if(l.getLessonName().equals(lessonName)) {
                return true;
            }
        }
        return false;
    }

    public void addInitialLessons() {

        LessonsCol.add(new Lesson("Yoga", 8));
        LessonsCol.add( new Lesson("Pilates", 6));
        LessonsCol.add( new Lesson("Zumba", 4));
        LessonsCol.add(new Lesson("CrossFit", 10));

    }

    public void addNewLesson(String lessName, float lessPrice) {

        LessonsCol.add(new Lesson(lessName,  lessPrice));
    }

    public void viewAllLessons() {
        for (Lesson l : LessonsCol) {
            System.out.println(l);
        }
    }


}
