package models;

//import POJO.Date;
import POJO.MyClass;

import java.util.*;

import static java.lang.Double.max;
import static java.util.Objects.isNull;

import java.util.Scanner;

public class MyClassModel {

    ArrayList<MyClass> timetable = new ArrayList<>();




    public void attendMyClass(int bookId)
    {

        for (MyClass c : timetable) {
            if(c.attendClass(bookId))
            {
                break;
            }

        }
    }
    public MyClass getRunningClass(String lessonName,int shift,String day,int weekNo,int monthNumber)
    {
        System.out.println("Lesson "+lessonName);
        System.out.println("shift "+shift);
        System.out.println("day "+day);
        System.out.println("weekNo "+weekNo);
        System.out.println("monthNumber "+monthNumber);

        for(MyClass c :timetable)
        {
            c.printMe();
            if (c.getLessonName().equals(lessonName) && c.getShift()==shift && c.getDay().equals(day) && c.getWeekNumber()==weekNo && c.getMonthNumber()==monthNumber)
                System.out.println("Found.....");
                return c;
        }
        return null;
    }


    //lessonAvailableByBook

    public boolean isLessonRunning(String lessonName,int shift,String day,int weekNo,int monthNumber)//day, month, shift, wek...
    {
        for(MyClass c : timetable)
        {
            if (c.lessonName.equals(lessonName) && c.getShift()==shift && c.getDay().equals(day)&& c.getWeekNumber()==weekNo && c.getMonthNumber()==monthNumber ) {

               return true;
            }
        }
        return false;
    }

    public boolean isSpaceAvailable(String lessonName,int shift,String day,int weekNo,int monthNumber)//day, month, shift, wek...
    {
        for(MyClass c : timetable)
        {
            if (c.lessonName.equals(lessonName) && c.getShift()==shift && c.getDay().equals(day)&& c.getWeekNumber()==weekNo && c.getMonthNumber()==monthNumber ) {

                return c.isSpaceAvailable();
            }
        }
        return false;
    }

    public boolean removeStudent_bookID(int bookID)
    {
        for(MyClass c : timetable)
        {
            if (c.removeStudentByBooking(bookID)) {
                return true;
            }
        }
        return false;
    }


    public boolean bookStudent(String lessonName,int studentID,int shift,String day,int weekNo,int monthNumber)
    {
        MyClass runningClass = getRunningClass(lessonName,shift,day,weekNo,monthNumber);
        if (isNull(runningClass))
        {
           System.out.println("The class is not running, please add class");
           return false;
        }
        System.out.println("The class is Running .......");
        if (!runningClass.isSpaceAvailable())
        {
            System.out.println("Space is not available");
            return false;
        }
        if(!isStudentAvailable(studentID,lessonName,shift,day,weekNo,monthNumber))
        {
            System.out.println("Student is not free");
            return false;
        }
        runningClass.addStudent(studentID);
        System.out.println("Student is added in the class");
        return true;

    }


    public boolean bookStudent(int bookId,String lessonName,int studentID,int shift,String day,int weekNo,int monthNumber)
    {
        MyClass runningClass = getRunningClass(lessonName,shift,day,weekNo,monthNumber);
        if (isNull(runningClass))
        {
            System.out.println("The class is not running, please add class");
            return false;
        }
        System.out.println("The class is Running .......");
        if (!runningClass.isSpaceAvailable())
        {
            System.out.println("Space is not available");
            return false;
        }
        if(!isStudentAvailable(studentID,lessonName,shift,day,weekNo,monthNumber))
        {
            System.out.println("Student is not free");
            return false;
        }
        runningClass.addStudent(bookId,studentID);
        System.out.println("Student is added in the class");
        return true;

    }

  /*  public Object getDay(String day){
        for(MyClass m: timetable){
            if(m.getDay()==day){
                return m;
            }
        }
        return null;
    }
    public Object getLessonName(String lessonName){
        for(MyClass m: timetable){
            if(m.getLessonName().equals(lessonName)){
                return m;
            }
        } return null;
    }

    public MyClass getMonthNumber(int monthNumber){
        for(MyClass m: timetable){
            if(m.getMonthNumber()==monthNumber){
                return m;
            }
        }return null;
    }

    public MyClass getWeekNumber(int weekNumber){
        for(MyClass m:timetable){
            if(m.getWeekNumber()==weekNumber){
                return m;
            }
        }return null;
    }
*/
//	public String studentName;
//	public int studentId;
//	public int bookingId;
//	public String bookingStatus

    public boolean removeStudentFromLesson(String lessonName,int studentID )
    {
        for(MyClass c:timetable)
        {
            if (c.getLessonName()==lessonName)
            {
                return c.removeStudent(studentID);
            }
        }
        return false;

    }
/*
    public void bookStudent(String lessonName,int studentId,int shift,String day,int weekNumber,int monthNumber)
    {

         boolean lessonFound = false;
        for(MyClass c:timetable)
        {

            if(c.getLessonName().equals(lessonName))
            {
                lessonFound = true;
                if(c.isSpaceAvailable())
                {
                    //add student
                    if(isStudentAvailable(studentId,lessonName,c.getShift(),c.getDay()))
                    {

                        c.addStudent(studentId);

                    }
                    else{

                        System.out.println("Student have some other booking for the lesson");
                    }
                }
                else
                {

                    System.out.println("There is no space available");
                    return;
                }
            }
        }
        if(!lessonFound)
        {
            System.out.println("Lesson is not running now...");
        }
    }

    isValidBookingID(bookId)


*/




    public int getStudentIdByBookId(int bookId)
    {

        for(MyClass c:this.timetable)
        {
            if (c.hasBookID(bookId))
            {
                return c.getStudentIdByBook(bookId);
            }
        }
        return -1;
    }


    public boolean isValidBookingID(int bookId)
    {

        for(MyClass c:this.timetable)
        {
           if (c.hasBookID(bookId))
            {
                return true;
            }
        }
        return false;
    }


    public boolean isStudentAvailable(int studentId,String lessonName,int shift,String day,int weekNo,int monthNo)
    {

        for(MyClass c:this.timetable)
        {
            if(c.hasStudent(studentId) && c.getLessonName().equals(lessonName) && c.shift==shift && c.day.equals(day) && c.getMonthNumber()==monthNo && c.getWeekNumber()==weekNo)
            {
                return false;
            }
        }
        return true;
    }



    public void addInitialClasses(){

        //Month 1
        //First month weekend 1 Saturday:

        MyClass Class1 = new MyClass("Yoga", 8,MyClass.MORNING,"Saturday",1, 1);
        Class1.addStudent(1);
        Class1.addStudent(2);
        timetable.add(Class1);

        MyClass Class2 = new MyClass("Yoga",8, MyClass.AFTERNOON, "Saturday",1, 1);
        Class2.addStudent(1);
        Class2.addStudent(5);
        Class2.addStudent(8);
        Class2.addStudent(6);
        timetable.add(Class2);

        MyClass Class3 = new MyClass("Yoga",8,MyClass.EVENING,"Saturday", 1,1);
        timetable.add(Class3);

        //First month weekend 1 Sunday:

        MyClass Class4 = new MyClass("Zumba",4, MyClass.MORNING, "Sunday",1,1);
        Class4.addStudent(3);
        Class4.addStudent(4);
        timetable.add(Class4);

        MyClass Class5 = new MyClass("Zumba",4, MyClass.AFTERNOON, "Sunday",1,1);
        Class5.addStudent(3);
        Class5.addStudent(8);
        Class5.addStudent(6);
        timetable.add(Class5);

        MyClass Class6 = new MyClass("Zumba", 4,MyClass.EVENING, "Sunday", 1, 1);
        Class6.addStudent(1);
        Class6.addStudent(9);
        Class6.addStudent(5);
        Class6.addStudent(8);
        timetable.add(Class6);

        //First month weekend 2 Saturday:

        MyClass Class7 = new MyClass("CrossFit",10, MyClass.MORNING, "Saturday",2,1);
        Class7.addStudent(1);
        Class7.addStudent(2);
        Class7.addStudent(3);
        Class7.addStudent(4);
        timetable.add(Class7);

        MyClass Class8 = new MyClass("CrossFit", 10, MyClass.AFTERNOON,"Saturday",2,1);
        Class8.addStudent(9);
        timetable.add(Class8);

        MyClass Class9 = new MyClass("CrossFit",10,MyClass.EVENING,"Saturday",2,1);
        Class9.addStudent(1);
        Class9.addStudent(3);
        Class9.addStudent(8);
        Class9.addStudent(6);
        timetable.add(Class9);

        //First month weekend 2 Sunday:

        MyClass Class10 = new MyClass("Zumba",4,MyClass.MORNING, "Sunday",2,1);
        Class10.addStudent(2);
        Class10.addStudent(4);
        Class10.addStudent(10);
        timetable.add(Class10);

        MyClass Class11 = new MyClass("Zumba",4, MyClass.AFTERNOON, "Sunday",2,1 );
        Class11.addStudent(7);
        timetable.add(Class11);

        MyClass Class12 = new MyClass("Zumba",4, MyClass.EVENING ,"Sunday",2,1 );
        timetable.add(Class12);

        //First month weekend 3 Saturday:

        MyClass Class13 = new MyClass("Yoga",8, MyClass.MORNING,"Saturday", 3,1);
        Class13.addStudent(7);
        Class13.addStudent(10);
        timetable.add(Class13);

        MyClass Class14= new MyClass("Yoga",8,MyClass.AFTERNOON ,"Saturday", 3, 1);
        timetable.add(Class14);

        MyClass Class15 = new MyClass("Yoga",8, MyClass.EVENING ,"Saturday",3, 1);
        timetable.add(Class15);

        //First month weekend 3 Sunday:

        MyClass Class16 = new MyClass("CrossFit",10, MyClass.MORNING, "Sunday", 3,1);
        timetable.add(Class16);

        MyClass Class17 = new MyClass("CrossFit",10,MyClass.AFTERNOON, "Sunday", 3, 1);
        Class17.addStudent(2);
        Class17.addStudent(3);
        Class17.addStudent(5);
        Class17.addStudent(4);
        timetable.add(Class17);

        MyClass Class18 = new MyClass("CrossFit",10,MyClass.EVENING, "Sunday",3, 1 );
        Class18.addStudent(1);
        Class18.addStudent(5);
        Class18.addStudent(9);
        Class18.addStudent(7);
        timetable.add(Class18);

        //First month weekend 4 Saturday:

        MyClass Class19 = new MyClass("Pilates",6,MyClass.MORNING,"Saturday", 4, 1);
        Class19.addStudent(5);
        Class19.addStudent(9);
        timetable.add(Class19);

        MyClass Class20 = new MyClass("Pilates",6,MyClass.AFTERNOON,"Saturday",4, 1);
        Class20.addStudent(10);
        Class20.addStudent(2);
        Class20.addStudent(7);
        timetable.add(Class20);

        MyClass Class21 = new MyClass("Pilates",6,MyClass.EVENING,"Saturday",4,1);
        Class21.addStudent(6);
        Class21.addStudent(3);
        Class21.addStudent(8);
        Class21.addStudent(4);
        timetable.add(Class21);


        //First month weekend 4 Sunday:

        MyClass Class22 = new MyClass("Zumba",4,MyClass.MORNING, "Sunday", 4,1);
        Class22.addStudent(2);
        Class22.addStudent(1);
        timetable.add(Class22);

        MyClass Class23 = new MyClass("Zumba",4, MyClass.AFTERNOON ,"Sunday",4,1);
        timetable.add(Class23);

        MyClass Class24 = new MyClass("Zumba",4,MyClass.EVENING ,"Sunday", 4,1);
        timetable.add(Class24);

         //month 2
        //Second month weekend 5 Saturday:

        MyClass Class25 = new MyClass("CrossFit", 10, MyClass.MORNING, "Saturday",5,2);
        Class25.addStudent(8);
        Class25.addStudent(6);
        Class25.addStudent(5);
        Class25.addStudent(10);
        timetable.add(Class25);

        MyClass Class26 = new MyClass("CrossFit", 10,MyClass.AFTERNOON, "Saturday",5,2);
        timetable.add(Class26);

        MyClass Class27 = new MyClass("CrossFit", 10, MyClass.EVENING, "Saturday",5,2);
        Class27.addStudent(8);
        timetable.add(Class27);

        //Second month weekend 5 Sunday:

        MyClass Class28 = new MyClass("Pilates", 6, MyClass.MORNING, "Sunday",5 ,2);
        Class28.addStudent(9);
        Class28.addStudent(2);
        Class28.addStudent(3);
        timetable.add(Class28);

        MyClass Class29 = new MyClass("Pilates", 6, MyClass.AFTERNOON, "Sunday", 5,2);
        Class29.addStudent(6);
        Class29.addStudent(3);
        Class29.addStudent(4);
        timetable.add(Class29);

        MyClass Class30 = new MyClass("Pilates",6, MyClass.EVENING, "Sunday",5,2);
        Class30.addStudent(2);
        Class30.addStudent(9);
        Class30.addStudent(8);
        timetable.add(Class30);

        //Second month weekend 6 Saturday:

        MyClass Class31 = new MyClass("Pilates",6, MyClass.MORNING, "Saturday",6,2);
        Class31.addStudent(3);
        Class31.addStudent(9);
        timetable.add(Class31);

        MyClass Class32 = new MyClass("Pilates",6, MyClass.AFTERNOON, "Saturday",6,2);
        Class32.addStudent(6);
        Class32.addStudent(8);
        timetable.add(Class32);

        MyClass Class33 = new MyClass("Pilates", 6, MyClass.EVENING,"Saturday",6,2);
        Class33.addStudent(4);
        Class33.addStudent(5);
        timetable.add(Class33);

        //Second month week 6 Sunday:

        MyClass Class34 = new MyClass("Yoga",8, MyClass.MORNING ,"Sunday",6,2);
        timetable.add(Class34);

        MyClass Class35 = new MyClass("Yoga",8, MyClass.AFTERNOON, "Sunday",6,2);
        Class35.addStudent(9);
        timetable.add(Class35);

        MyClass Class36 = new MyClass("Yoga", 8, MyClass.EVENING, "Sunday",6,2);
        Class36.addStudent(8);
        timetable.add(Class36);

        //Second month weekend 7 Saturday:

        MyClass Class37 = new MyClass("Zumba" ,4, MyClass.MORNING ,"Saturday", 7,2);
        timetable.add(Class37);

        MyClass Class38 = new MyClass("Zumba",4, MyClass.AFTERNOON, "Saturday",7,2);
        Class38.addStudent(1);
        Class38.addStudent(10);
        timetable.add(Class38);

        MyClass Class39 = new MyClass("Zumba", 4, MyClass.EVENING, "Saturday",7,2);
        Class39.addStudent(2);
        Class39.addStudent(4);
        Class39.addStudent(6);
        Class39.addStudent(8);
        timetable.add(Class39);

        //Second month weekend 7 Sunday:

        MyClass Class40 = new MyClass("Pilates",6, MyClass.MORNING, "Sunday", 7,2);
        Class40.addStudent(10);
        timetable.add(Class40);

        MyClass Class41 = new MyClass("Pilates",6, MyClass.AFTERNOON, "Sunday",7,2);
        Class41.addStudent(6);
        Class41.addStudent(9);
        timetable.add(Class41);

        MyClass Class42 = new MyClass("Pilates",6,MyClass.EVENING, "Sunday",7,2);
        Class42.addStudent(1);
        Class42.addStudent(3);
        Class42.addStudent(6);
        Class42.addStudent(4);
        timetable.add(Class42);


        //Second month weekend 8 Saturday:

        MyClass Class43 = new MyClass("CrossFit",10, MyClass.MORNING,"Saturday",8,2);
        Class43.addStudent(7);
        timetable.add(Class43);

        MyClass Class44 = new MyClass("CrossFit",10, MyClass.AFTERNOON, "Saturday",8,2);
        Class44.addStudent(3);
        Class44.addStudent(5);
        Class44.addStudent(7);
        Class44.addStudent(9);
        timetable.add(Class44);

        MyClass Class45 = new MyClass("CrossFit",10, MyClass.EVENING,"Saturday",8,2);
        Class45.addStudent(7);
        timetable.add(Class45);

        //Second month weekend 8 Sunday:

        MyClass Class46 = new MyClass("Yoga",8, MyClass.MORNING, "Sunday", 8,2);
        Class46.addStudent(5);
        timetable.add(Class46);

        MyClass Class47 = new MyClass("Yoga",8, MyClass.AFTERNOON, "Sunday",8, 2);
        timetable.add(Class47);

        MyClass Class48 = new MyClass("Yoga", 8, MyClass.EVENING, "Sunday",8,2);
        Class48.addStudent(2);
        Class48.addStudent(9);
        timetable.add(Class48);

    }






//view all timetable
    public void viewALLClasses(StudentModel sModel) {
        System.out.println("please enter the month number:");
        Scanner scanner = new Scanner(System.in);


        for (MyClass t : timetable) {

            t.printMe(sModel);
        }
    }

    public void viewSatClasses(StudentModel sModel, String uday) {


            for (MyClass t : timetable) {

                if(t.day == "Saturday")
                t.printMe(sModel);

            }

    }

    public void viewYogaClasses(StudentModel sModel, String ExName) {

        for (MyClass t : timetable) {

            if(t.lessonName == "Yoga")
                t.printMe(sModel);

        }

    }
    public void viewZumbaClasses(StudentModel sModel, String ExName) {

        for (MyClass t : timetable) {

            if(t.lessonName == "Zumba")
                t.printMe(sModel);

        }

    }
    public void viewCrossFitClasses(StudentModel sModel, String ExName) {


        for (MyClass t : timetable) {

            if(t.lessonName == "CrossFit")
                t.printMe(sModel);

        }

    }
    public void viewPilatesClasses(StudentModel sModel, String ExName) {


        for (MyClass t : timetable) {

            if(t.lessonName == "Pilates")
                t.printMe(sModel);

        }

    }

    public void viewSunClasses(StudentModel sModel, String uday) {


        for (MyClass t : timetable) {

            if(t.day == "Sunday")
                t.printMe(sModel);

        }

    }




public void viewReport1(StudentModel sModel) {

    System.out.println("please enter the month number:");
    Scanner scanner = new Scanner(System.in);
    int month_number = scanner.nextInt();
    System.out.println("generating the monthly lesson report for month " +month_number+ "\n");
    for (MyClass t : timetable) {


        t.printReport2(sModel, month_number);

    }

}





    public void viewReport2 (StudentModel sModel) {
        int zumba = 0;
        int yoga = 0;
        int pilates = 0;
        int crossfit = 0;
        int less[] = new int[timetable.size()];

        System.out.println("please enter the month number:");
        Scanner scanner = new Scanner(System.in);
        int month_num = scanner.nextInt();



        for (MyClass t : timetable) {

            
            t.printReport2(sModel, month_num);


            if (Objects.equals(t.lessonName, "Zumba") && (t.monthNumber==month_num)) {
                //zumba++;
                less[1]++;
            }
            else if (Objects.equals(t.lessonName, "CrossFit" )&& (t.monthNumber==month_num)) {
                //crossfit++;
                less[2]++;
            }
            else if (Objects.equals(t.lessonName, "Pilates")&& (t.monthNumber==month_num)) {
                //pilates++;
                less[3]++;
            }
            else if (Objects.equals(t.lessonName, "yoga")&& (t.monthNumber==month_num)) {
                //yoga++;
                less[4]++;
            }
        }




        int max1 = (int) Math.max(less[1],max(less[2],less[3]));
        int maxfinal = Math.max(max1,less[4]);



        if (less[1] == maxfinal){
            System.out.println("\nThe highest income is from Zumba with total lessons "+maxfinal+ " earning £" +(maxfinal*4)+".\n");
        }
       else if (less[2] == maxfinal){
            System.out.println("\nThe highest income is from CrossFit with total lessons " +maxfinal+ " earning £"+(maxfinal*10)+".\n");
        }
        else if (less[3] == maxfinal){
            System.out.println("\nThe highest income is from Pilates with total lessons " +maxfinal+ " earning £"+(maxfinal*6)+".\n");
        }
        else if (less[4] == maxfinal){
            System.out.println("\nThe highest income is from Yoga with total lessons " +maxfinal+ " earning £"+(maxfinal*8)+".\n");
        }

    }

   // public void addClasses(int lessonId, int shift, String day, int weekNumber, int monthNumber) {
    //}







}
