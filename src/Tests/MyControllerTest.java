package Tests;
import java.util.Scanner;

import models.LessonModel;
import models.MyClassModel;
import models.MyController;
import models.StudentModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyControllerTest {

   public models.MyController MyController = new MyController();
   public MyClassModel cModel= new MyClassModel();
   public StudentModel sModel= new StudentModel();
   public LessonModel lModel = new LessonModel();
   Scanner scanner;
    @Test
    void removeStudentFromClass() {
        System.out.println("Enter  bookingId: ");
        int bookingId=scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter lessonName: ");
        String lessonName = scanner.nextLine();

        System.out.println("Enter StudentID: ");
        int id = scanner.nextInt();
        if (cModel.isValidBookingID(bookingId)){

        } else {
            System.out.println("This booking ID doesn't exist!");
        }
        //check is student registered
        if(!sModel.isStudentRegistered(id))
        {
            System.out.println("This student with id "+id+ " is not found");
            return;
        }
        //check is lesson running or not

        if(!lModel.isLessonRegistered(lessonName))
        {
            System.out.println("This lesson "+lessonName+" is not registered..");
            return;
        }

        boolean canRemove = cModel.removeStudentFromLesson(lessonName,id);
        if(canRemove)
        {
            System.out.println("Student ID "+id+" is removed from Lesson"+lessonName);
        } else
        {
            System.out.println("Something is wrong");

        }
        //Assertions.assertTrue(models.MyController.removeStudentFromClass());

    }

    }
