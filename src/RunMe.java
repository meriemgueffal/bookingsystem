import java.util.Scanner;

import models.*;




public class RunMe {

    public static void main(String[] abc)
    {
        Scanner scanner = new Scanner(System.in);
        MyController myController = new MyController();

        while(true)
        {
            System.out.println("Welcome to The University Sports Centre (USC). \nPlease choose an option from the menu:\n ");
            System.out.println("********************************************");
            System.out.println("1. Book a group exercise lesson.");
            System.out.println("2. Change (or cancel) a booking.");
            System.out.println("3. Attend (or review) a lesson.");
            System.out.println("4. Monthly lesson report.");
            System.out.println("5. Monthly champion exercise report.");
            System.out.println("********************************************");
            System.out.println("6. List all students.");
            System.out.println("7. List all lessons.");
            System.out.println("8. List all classes.");
            System.out.println("9. List all reviews.");
            System.out.println("********************************************");

            System.out.println("10. Exit.");

            int choice = scanner.nextInt();
            if(choice==10)
            {
                break;
            }
            switch(choice)
            {
                case 1:

                    myController.bookLesson();
                    break;

                case 2:

                    while(true){
                        System.out.println("\nPlease choose an option from the menu below: ");
                        System.out.println("1. Change a booking.");
                        System.out.println("2. Cancel a booking.");
                        System.out.println("3. Exit.");

                        int choice1 = scanner.nextInt();
                        if(choice1==3) {
                            break;
                        }
                        switch (choice1){
                            case 1:

                                //changing a booking code
                                myController.changeBooking();



                                break;
                            case 2:

                                myController.removeStudentFromClass();
                                break;
                            default:
                                System.out.println("Invalid input...");
                        }
                    }

                    break;
                case 3:

                    while(true){
                        System.out.println("\nPlease choose an option from the menu below: ");
                        System.out.println("1. Add a review.");
                        System.out.println("2. Attend a booking.");
                        System.out.println("3. Exit.");

                        int choice1 = scanner.nextInt();
                        if(choice1==3) {
                            break;
                        }
                        switch (choice1){
                            case 1:

                                myController.addReview();
                                break;
                            case 2:
                              System.out.println("Please input your booking Id");
                              int id=scanner.nextInt();

                              myController.attendClass(id);
                                break;
                            default:
                                System.out.println("Invalid input...");
                        }
                    }
                    break;

                case 4:
                    myController.viewClassReport();

                    break;
                    

                case 5:
                    myController.report2();
                    break;

                case 6:
                    myController.viewAllStudents();
                    break;

                case 7:
                    myController.viewAllLessons();
                    break;

                case 8:
                    myController.viewALLClasses();
                    break;

                case 9:
                    myController.reviews();
                    break;


                default:
                    System.out.println("Invalid input...");

            }

        }
    }

}
