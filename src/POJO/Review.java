package POJO;

public class Review {
    int studentId;
    String lessonName;
    int rating;
    String reviewText;

    public String getLessonName() {

        return this.lessonName;
    }
    public Integer getRating(){
        return this.rating;
    }

    public  Review(int studentId,String lessonName, int rating , String reviewText) {
            this.studentId=studentId;
            this.lessonName=lessonName;
            this.rating=rating;
            this.reviewText=reviewText;


    }







    public String toString() {
      System.out.println("Student ID: " +this.studentId+ ". Lesson name: " + this.lessonName + ". Rating: " + this.rating + ". Review: " +this.reviewText);
      return "";
    }

}


