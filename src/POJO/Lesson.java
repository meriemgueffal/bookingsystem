package POJO;

public class Lesson {

    public String lessonName;
    public double lessonPrice;

    public String getLessonName() {

        return this.lessonName;
    }

    public Lesson(String lessName, float lessPrice){

        lessonName = lessName;
        lessonPrice = lessPrice;
    }

    public String toString() {

        return "Lesson Name :" + this.lessonName+"  Price: " +this.lessonPrice;
    }



}
