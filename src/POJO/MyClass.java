package POJO;

import models.*;

import java.util.Scanner;


public class MyClass {



	public static int MORNING = 10;
	public static int AFTERNOON = 11;
	public static int EVENING = 12;

	final int MAX_STUDENT_NUMBERS=4;

	public static int bookingID;
	//public String[] bookingStatus={"Booked", "Attended", "Changed", "Cancelled"};


	public String lessonName;
	public int lessonPrice;
	public int  studentId[] = new int[MAX_STUDENT_NUMBERS];
	public int bookingId[]={-1,-1,-1,-1};  //-1 means there is no student
	public int bookingStatus[]={-1,-1,-1,-1};  //-1 means not occupied
	public int numberStudents;
	public int shift;
	public String day;
    public int weekNumber;
	public int monthNumber;


	double allPayment[]={0.0,0.0,0.0,0.0};

	public int getStudentIdByBook(int id)
	{
		for(int i=0;i<MAX_STUDENT_NUMBERS;i++) {
			if (this.bookingId[i] == id) {
				return this.studentId[i];
			}
		}
		return -1;

	}

	public boolean removeStudent(int id)
	{

		for(int i=0;i<MAX_STUDENT_NUMBERS;i++)
		{
			if(this.studentId[i]==id)
			{

				for(int j=i+1;j<this.numberStudents;j++) {
					studentId[j-1]=studentId[j];
					bookingId[j-1]=bookingId[j];
					bookingStatus[j-1]=bookingStatus[j];


					this.numberStudents--;


				}
				studentId[this.numberStudents]=-1;//erase information
				bookingId[this.numberStudents]=-1;//erase information
				bookingStatus[this.numberStudents]=-1;//erase information
				return true;
			}
		}
		return false;
	}

	public boolean removeStudentByBooking(int id)
	{

		for(int i=0;i<MAX_STUDENT_NUMBERS;i++)
		{
			if(this.bookingId[i]==id)
			{

				for(int j=i+1;j<this.numberStudents;j++) {
					studentId[j-1]=studentId[j];
					bookingId[j-1]=bookingId[j];
					bookingStatus[j-1]=bookingStatus[j];


					this.numberStudents--;


				}
				studentId[this.numberStudents]=-1;//erase information
				bookingId[this.numberStudents]=-1;//erase information
				bookingStatus[this.numberStudents]=-1;//erase information
				return true;
			}
		}
		return false;
	}




	public boolean attendClass(int id)
	{
		for(int i=0;i<numberStudents;i++)
		{
			if(this.bookingId[i]==id)
			{
				this.bookingStatus[i]=1;
				System.out.println("Status changed from 'Booked' to Attended'. You have confirmed your attendance");
				return true;
			}
		}
		return false;

	}


	/* public boolean hasAttendClass(int bID)
	{
		for(int i=0;i<numberStudents;i++)
		{
			if(this.bookingId[i]==bID)
			{
				return this.bookingStatus[i]==1;

			}
		}
		return false;

	} */

	public boolean hasStudent(int id)
	{
		for(int i=0;i<numberStudents;i++)
		{
			if(studentId[i]==id)
			{
				return true;
			}
		}
		return false;
	}

	public int getShift()
	{
		return this.shift;
	}

	public String getLessonName()
	{

		return this.lessonName;
	}
	public String getDay()
	{


		return this.day;
	}

	public int getWeekNumber(){
		return this.weekNumber;
	}
	public int getMonthNumber(){
		return this.monthNumber;
	}



	 public boolean isSpaceAvailable()
	{
		return numberStudents<MAX_STUDENT_NUMBERS;
	}




	public MyClass(String lessonName,int lessonPrice,int shift,String day,int weekNumber,int monthNumber) {
		this.lessonName=lessonName;
		this.lessonPrice=lessonPrice;

		this.numberStudents=0;
        this.shift=shift;
		this.day=day;

        this.weekNumber=weekNumber;
		this.monthNumber=monthNumber;
		MyClass.bookingID=1;

	}



	public boolean addStudent(int studentID) {
		if(numberStudents<4) {
			studentId[numberStudents]=studentID;

			this.bookingId[numberStudents]=MyClass.bookingID++;//1 means occupied
			this.bookingStatus[numberStudents]=1;//1 means occupied
			numberStudents++;
			return true;
		}
		return false;
	}


	public boolean addStudent(int bookingID,int studentID) {
		if(numberStudents<4) {
			studentId[numberStudents]=studentID;

			this.bookingId[numberStudents]=bookingID;// using old booking ID
			this.bookingStatus[numberStudents]=1;//1 means occupied
			numberStudents++;
			return true;
		}
		return false;
	}

	/* public boolean addPayment(int studentID,double payment) {
		for(int i=0;i<=numberStudents;i++) {
			if(studentId[i]==studentID) {
				allPayment[i]+=payment;
				return true;
			}
		}
		return false;
	}

	public double getStudentPayment(int studentID) {
		for(int i=0;i<=numberStudents;i++) {
			if(studentId[i]==studentID) {
				return allPayment[i];

			}
		}
		return -1;
	}

	public void viewAllStudentIDs()
	{
		for(int i=0;i<this.numberStudents;i++)
		{
			System.out.println(this.studentId[i]);
		}
	}
*/
	public boolean hasBookID(int bookID)
	{
		for(int id :this.bookingId)
		{
			if (id==bookID)
				return true;
		}
		return false;
	}




	public void printMe(StudentModel myStudentM)

	{

		String newShift = new String();
		String newDay = new String();
		//this.monthNumber=monthNumber;

		if (this.shift == 10 ){
			newShift = "Morning";
		}
		else if(this.shift == 11){
			newShift = "Afternoon";
		}
		else if(this.shift == 12){
			newShift = "Evening";
		}
		if (this.day == "Saturday"){
			newDay = "Saturday";
		}
		else if(this.day == "Sunday"){
			newDay = "Sunday";
		}

			System.out.println("\nLesson Name: " + this.lessonName + " Day & Shift : " + newDay + " " + newShift + ". Month " + this.monthNumber + " ,Week " + this.weekNumber + ". Number of Students:" + this.numberStudents);

			System.out.println("List of Students: ");
			for (int i = 0; i < this.numberStudents; i++) {

				Student s = myStudentM.getStudent(this.studentId[i]);
				try {
					System.out.println("ID: " + s.studentId + " -Name: " + s.studentName + ".");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

	}



	public void printMe()

	{

		String newShift = new String();
		String newDay = new String();
		//this.monthNumber=monthNumber;

		if (this.shift == 10 ){
			newShift = "Morning";
		}
		else if(this.shift == 11){
			newShift = "Afternoon";
		}
		else if(this.shift == 12){
			newShift = "Evening";
		}
		if (this.day == "Saturday"){
			newDay = "Saturday";
		}
		else if(this.day == "Sunday"){
			newDay = "Sunday";
		}

		System.out.println("\nLesson Name: " + this.lessonName + " Day & Shift : " + newDay + " " + newShift + ". Month " + this.monthNumber + " ,Week " + this.weekNumber + ". Number of Students:" + this.numberStudents);

		System.out.println("Number of students: "+this.numberStudents);


	}



	public void printReport2(StudentModel sModel, int monthNumber)
	{
		String newShift = new String();
		String newDay = new String();
		int newmonth;

		int zumba = 0;
		int yoga;
		int crossfit;
		int pilates;


			if (this.shift == 10) {
				newShift = "Morning";
			} else if (this.shift == 11) {
				newShift = "Afternoon";
			} else if (this.shift == 12) {
				newShift = "Evening";
			}
			if (this.day == "Saturday") {
				newDay = "Saturday";
			} else if (this.day == "Sunday") {
				newDay = "Sunday";
			}


			if(this.monthNumber==monthNumber){

				System.out.println("The class " + this.lessonName + " ran on " + newDay + " " + newShift + " week " + this.weekNumber + " month " + this.monthNumber);
			}



	}


	
}




